# Skryba Demo - My Birthday Blog

A simple static blog demonstrating the use of: tags, images (`img`), links (`a`) and multiple languages (i18n).

Check `.gitlab-ci.yml` for instructions.

Watch it live here: https://skrybagr.gitlab.io/demo-my-birthday-blog/ .
